﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Forms;

namespace ComputerGraphics
{
    static class Algorithms
    {
        public enum PointSegmentRelation
        {
            Lies,
            Left,
            Right
        }
        
        public static Point Apply(this Point point, AffineMatrix matrix)
        {
            point = point.Copy();
            float[] newPosition = matrix.Multiply(point.x, point.y, 1);
            point.x = newPosition[0];
            point.y = newPosition[1];
            return point;
        }
        
        public static Segment Apply(this Segment segment, AffineMatrix matrix)
        {
            segment = segment.Copy();
            segment.point1 = Apply(segment.point1, matrix);
            segment.point2 = Apply(segment.point2, matrix);
            return segment;
        }

        public static Segment Reverse(this Segment segment)
        {
            return new Segment(segment.point2, segment.point1);
        }
        
        public static Polygon Apply(this Polygon polygon, AffineMatrix matrix)
        {
            polygon = polygon.Copy();
            LinkedListNode<Segment> node = polygon.segments.First;
            while (node != null)
            {
                node.Value = node.Value.Apply(matrix);
                node = node.Next;
            }
            return polygon;
        }

        public static Polygon Reverse(this Polygon polygon)
        {
            LinkedList<Segment> reversedSegments = new LinkedList<Segment>();
            LinkedListNode<Segment> node = polygon.segments.Last;
            while (node != null)
            {
                reversedSegments.AddLast(node.Value.Reverse());
                node = node.Previous;
            }
            return new Polygon(reversedSegments);
        }
        
        static public Polygon ForceClockwise(this Polygon polygon)
        {
            polygon = polygon.Copy();
            LinkedListNode<Segment> node = polygon.segments.First;
            int lefts = 0;
            int rights = 0;
            while (node != null)
            {
                Segment currentSegment = node.Value;
                Segment nextSegment = (node.Next ?? node.List.First).Value;
                Point nextPoint = nextSegment.point2;
                switch (PointSegmentPosition(currentSegment, nextPoint))
                {
                    case PointSegmentRelation.Left:
                        lefts++;
                        break;
                    case PointSegmentRelation.Right:
                        rights++;
                        break;
                }
                node = node.Next;
            }
            if (lefts > rights)
            {
                polygon = polygon.Reverse();
            }
            return polygon;
        }

        static public bool IsIntersection(Point A, Point B, Point C, Point D, Point P)
        {
            return ((A.x <= P.x && B.x >= P.x && C.x <= P.x && D.x >= P.x)
                    || (A.y <= P.y && B.y >= P.y && C.y <= P.y && D.y >= P.y)
                    || (B.y <= P.y && A.y >= P.y && C.y <= P.y && D.y >= P.y)
                    || (A.y <= P.y && B.y >= P.y && D.y <= P.y && C.y >= P.y))
                   && ((B.x - A.x) / (B.y - A.y)) != ((D.x - C.x) / (D.y - C.y));
        }

        static public Point Intersection(Point A, Point B, Point C, Point D)
        {
            Vector a = new Vector(A.x, A.y);
            Vector b = new Vector(B.x, B.y);
            Vector c = new Vector(C.x, C.y);
            Vector d = new Vector(D.x, D.y);
            Vector cd = new Vector(D.x - C.x, D.y - C.y);
            Vector n = new Vector(-1 * cd.Y, cd.X);     // нормаль к вектору cd

            double t = -1 * (n * (a - c)) / (n * (b - a));
            Vector pt = a + t * (b - a);
            return new Point((float)pt.X, (float)pt.Y);
        }

        static public Point SimpleIntersection(Point A, Point B, Point C, Point D)
        {
            float Ax = A.x, Ay = A.y;
            float ABx = B.x - A.x, ABy = B.y - A.y;

            float Cx = C.x, Cy = C.y;
            float CDx = D.x - C.x, CDy = D.y - C.y;

            float x = (Ax * ABy * CDx - Cx * CDy * ABx - Ay * ABx * CDx + Cy * ABx * CDx) /
                      (ABy * CDx - CDy * ABx);
            float y = (Ay * ABx * CDy - Cy * CDx * ABy - Ax * ABy * CDy + Cx * ABy * CDy) /
                      (ABx * CDy - CDx * ABy);

            return new Point(x, y);
        }

        static public PointSegmentRelation PointSegmentPosition(this Segment segment, Point point)
        {
            AffineMatrix matrix = AffineMatrices.Translate(segment.point1.x, segment.point1.y);
            point = point.Apply(matrix);
            segment = segment.Apply(matrix);
            float f = point.y * segment.point2.x - point.x * segment.point2.y;
            if (f > 0)
                return PointSegmentRelation.Right;
            else if (f < 0)
                return PointSegmentRelation.Left;
            else
                return PointSegmentRelation.Lies;
        }

        static public PointSegmentRelation PointSegmentPosition(this Point point, Segment segment)
        {
            return segment.PointSegmentPosition(point);
        }

        static public bool PointInConvexPolygon(this Point point, Polygon polygon)
        {
            polygon = polygon.ForceClockwise();
            foreach (Segment segment in polygon.segments)
                if (segment.PointSegmentPosition(point) == PointSegmentRelation.Left)
                    return false;
            return true;
        }

        static public bool PointInConvexPolygon(this Polygon polygon, Point point)
        {
            return point.PointInConvexPolygon(polygon);
        }
        static public bool PointInPolygon(this Point point, Polygon polygon)
        {
            float anglesSum = 0;
            foreach (Segment segment in polygon.segments)
            {
                Point point1 = segment.point1.Apply(AffineMatrices.Translate(point.x, point.y));
                Point point2 = segment.point2.Apply(AffineMatrices.Translate(point.x, point.y));
                float dot = point1.x * point2.x + point1.y * point2.y;
                float cross = point1.x * point2.y - point1.y * point2.x;
                float length1 = (float)Math.Sqrt(point1.x * point1.x + point1.y * point1.y);
                float length2 = (float) Math.Sqrt(point2.x * point2.x + point2.y * point2.y);
                float angleCos = dot / (length1 * length2);
                float angleSin = cross / (length1 * length2);
                float angle = (float) (Math.Acos(angleCos) * 180 / Math.PI * Math.Sign(angleSin));
                anglesSum += angle;
            }
            return Math.Abs(Math.Abs(anglesSum) - 360) <= 0.1;
        }

        static public bool PointInPolygon(this Polygon polygon, Point point)
        {
            return point.PointInPolygon(polygon);
        }

        static public bool IsConvex(this Polygon polygon)
        {
            polygon = polygon.ForceClockwise();
            LinkedListNode<Segment> node = polygon.segments.First;
            int lefts = 0;
            while (node != null)
            {
                Segment currentSegment = node.Value;
                Segment nextSegment = (node.Next ?? node.List.First).Value;
                Point nextPoint = nextSegment.point2;
                if (PointSegmentPosition(currentSegment, nextPoint) == PointSegmentRelation.Left)
                    lefts++;
                node = node.Next;
            }
            if (lefts != 0)
                return false;
            return true;
        }

        static public float SegmentLength(this Segment segment)
        {
            float diffx = segment.point2.x - segment.point1.x;
            float diffy = segment.point2.y - segment.point1.y;
            return (float) Math.Sqrt(diffx * diffx + diffy * diffy);
        }
        
        static public float PointSegmentDistance(this Point point, Segment segment)
        {
            point = point.Apply(AffineMatrices.Translate(segment.point1.x, segment.point1.y));
            segment = segment.Apply(AffineMatrices.Translate(segment.point1.x, segment.point1.y));
            float segmentAngleCos = segment.point2.x / segment.SegmentLength();
            float segmentAngleSin = segment.point2.y / segment.SegmentLength();           
            float rotationAngle = (float) (Math.Acos(segmentAngleCos) * 180 / Math.PI
                                           * Math.Sign(Math.Asin(segmentAngleSin)));
            point = point.Apply(AffineMatrices.Rotate(rotationAngle));
            return Math.Abs(point.y);
        }
        
        static public List<Polygon> Triangulate(this Polygon polygon)
        {
            polygon = polygon.ForceClockwise();
            if (polygon.segments.Count == 3)
                return new List<Polygon>(new[] {polygon});
            List<Polygon> triangles = new List<Polygon>();
            // Ищем выпуклую вершину.
            LinkedListNode<Segment> currentNode = polygon.segments.First;
            Segment currentSegment = currentNode.Value;
            LinkedListNode<Segment> nextNode = currentNode.Next ?? currentNode.List.First;
            Segment nextSegment = nextNode.Value;
            while (true)
            {
                if (nextSegment.point2.PointSegmentPosition(currentSegment) != PointSegmentRelation.Left)
                    break;
                currentNode = nextNode;
                currentSegment = currentNode.Value;
                nextNode = nextNode.Next ?? nextNode.List.First;
                nextSegment = nextNode.Value;
            }
            // Строим треугольник.
            Segment diagonal = new Segment(nextSegment.point2, currentSegment.point1);
            Polygon triangle = new Polygon(new[] {currentSegment, nextSegment, diagonal});
            // Ищем вторгающуюся вершину.
            Point invadingPoint = null;
            Segment invadingPointNextSegment = null;
            float pointDiagonalDistance = 0;
            foreach (Segment segment in polygon.segments)
            {
                Point point = segment.point1;
                if (point == currentSegment.point1 || point == nextSegment.point1 || point == diagonal.point1)
                    continue;
                if (point.PointInConvexPolygon(triangle))
                    if (point.PointSegmentDistance(diagonal) > pointDiagonalDistance)
                    {
                        invadingPoint = point;
                        invadingPointNextSegment = segment;
                        pointDiagonalDistance = point.PointSegmentDistance(diagonal);
                    }
            }
            // Если вторгающейся вершины нет, то отсекаем треугольник и триангулируем остальную часть.
            if (invadingPoint == null)
            {
                triangles.Add(triangle);
                polygon.segments.AddAfter(currentNode, diagonal.Reverse());
                polygon.segments.Remove(currentNode);
                polygon.segments.Remove(nextNode);
                triangles.AddRange(polygon.Triangulate());
            }
            // Иначе разбиваем полигон на два и триангулируем каждую часть.
            else
            {
                // Одна часть.
                // Вначале проходим от вторгающейся вершины до найденной выпуклой вершины.
                LinkedListNode<Segment> invadingPointNextNode = polygon.segments.Find(invadingPointNextSegment);
                LinkedList<Segment> segments1 = new LinkedList<Segment>();
                LinkedListNode<Segment> node = invadingPointNextNode;
                while (node != nextNode)
                {
                    segments1.AddLast(node.Value);
                    node = node.Next ?? node.List.First;
                }
                // Добавляем отрезок (выпуклая вершина; вторгающаяся вершина).
                segments1.AddLast(new Segment(node.Value.point1, invadingPoint));
                Polygon polygon1 = new Polygon(segments1);
                // Другая часть.
                // Затем проходим от выпуклой вершины до вторгающейся. 
                LinkedList<Segment> segments2 = new LinkedList<Segment>();
                while (node != invadingPointNextNode)
                {
                    segments2.AddLast(node.Value);
                    node = node.Next ?? node.List.First;
                }
                // Добавляем отрезок (вторгающаяся вершина; выпуклая вершина).
                segments2.AddLast(segments1.Last.Value.Reverse());
                Polygon polygon2 = new Polygon(segments2);
                // Триангулируем каждую часть.
                triangles.AddRange(polygon1.Triangulate());
                triangles.AddRange(polygon2.Triangulate());
            }
            return triangles;
        }
    }
}