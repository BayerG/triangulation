﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Runtime.CompilerServices;

namespace ComputerGraphics
{
    class AffineMatrix
    {
        public float[,] items;

        public AffineMatrix()
        {
            this.items = new float[3, 3];
        }
        
        public AffineMatrix(float[,] items)
        {
            this.items = items;
        }

        public AffineMatrix(params float[] items)
        {
            this.items = new float[3, 3];
            for (int i = 0; i < 9; i++)
                this.items[i / 3, i % 3] = items[i];
        }

        public float[] Multiply(params float[] vector)
        {
            // vector * this - умножение слева
            float[] result = new float[3];
            for (int n = 0; n < 3; n++)
                for (int m = 0; m < 3; m++)
                    result[n] += vector[m] * this.items[m, n];
            return result;
        }

        public AffineMatrix Multiply(AffineMatrix matrix)
        {
            // this * matrix - умножение справа
            float[,] result = new float[3, 3];
            for (int m = 0; m < 3; m++)
                for (int n = 0; n < 3; n++)
                    for (int t = 0; t < 3; t++)
                        result[m, n] += this.items[m, t] * matrix.items[t, n];
            return new AffineMatrix(result);
        }

        public AffineMatrix Translate(float dx, float dy)
        {
            return this.Multiply(AffineMatrices.Translate(dx, dy));
        }

        public AffineMatrix Rotate(float angle)
        {
            return this.Multiply(AffineMatrices.Rotate(angle));
        }

        public AffineMatrix Scale(float kx, float ky)
        {
            return this.Multiply(AffineMatrices.Scale(kx, ky));
        }
    }

    static class AffineMatrices
    {
        public static AffineMatrix Translate(float dx, float dy)
        {
            return new AffineMatrix(
                1, 0, 0,
                0, 1, 0,
                -dx, -dy, 1);
        }

        public static AffineMatrix Rotate(double angle)
        {
            float angleRadians = (float)(angle * Math.PI / 180);
            float sin = (float)Math.Sin(angleRadians);
            float cos = (float)Math.Cos(angleRadians);
            return new AffineMatrix(
                cos, sin, 0,
                -sin, cos, 0,
                0, 0, 1);
        }

        public static AffineMatrix Scale(float kx, float ky)
        {
            return new AffineMatrix(
                1/kx, 0, 0,
                0, 1/ky, 0,
                0, 0, 1);
        }
    }
}