﻿using System;
using System.Windows.Forms;
using System.Drawing;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace ComputerGraphics
{
    public enum Mode
    {
        Select, AddPoint, AddSegment, AddPolygon
    }

    public partial class MainForm : Form
    {
        public Mode mode = Mode.Select;
        public int clickCount = 0;
        public Graphics graphics;
        Point prevPoint;
        LinkedList<Segment> segments;
        
        public MainForm()
        {
            InitializeComponent();
            Image image = new Bitmap(5000, 5000);
            graphics = Graphics.FromImage(image);
            pictureBox.Image = image;
        }

        private void AddPointClick(object sender, System.EventArgs e)
        {
            mode = Mode.AddPoint;
            DisableButtons();
        }

        private void AddSegmentClick(object sender, System.EventArgs e)
        {
            mode = Mode.AddSegment;
            DisableButtons();
        }

        private void AddPolygonClick(object sender, System.EventArgs e)
        {
            mode = Mode.AddPolygon;
            DisableButtons();
        }

        private void ClearClick(object sender, System.EventArgs e)
        {
            mode = Mode.Select;
            Clear();
        }

        private void Redraw()
        {
            graphics.Clear(Color.Transparent);
            foreach (IPrimitive primitive in primitivesListBox.Items)
                primitive.Draw(graphics);
            foreach (IPrimitive primitive in primitivesListBox.SelectedItems)
                primitive.DrawSelection(graphics);
            pictureBox.Refresh();
        }

        private void primitivesListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListBox.SelectedObjectCollection selectedItems = primitivesListBox.SelectedItems;
            Redraw();
            if (selectedItems.Count == 1)
            {
                statusLabel.Text = "";
                if (selectedItems[0] is Polygon)
                {
                    groupBox2.Enabled = true;
                    groupBox3.Enabled = true;
                    groupBox4.Enabled = true;
                    groupBox7.Enabled = true;
                }
                else if (selectedItems[0] is Segment)
                {
                    groupBox5.Enabled = true;
                    groupBox6.Enabled = false;
                }               
                else
                {
                    groupBox2.Enabled = false;
                    groupBox3.Enabled = false;
                    groupBox4.Enabled = false;
                    groupBox5.Enabled = false;
                    groupBox6.Enabled = false;
                    groupBox7.Enabled = false;
                }
            }
            else if (selectedItems.Count == 2)
                if (selectedItems[0] is Segment
                    && selectedItems[1] is Point
                    || selectedItems[0] is Point
                    && selectedItems[1] is Segment)
                {
                    groupBox2.Enabled = false;
                    groupBox3.Enabled = false;
                    groupBox4.Enabled = false;
                    statusLabel.Text = PointSegmentStatus();
                }
                else if (selectedItems[0] is Polygon
                    && selectedItems[1] is Point
                    || selectedItems[0] is Point
                    && selectedItems[1] is Polygon)
                {
                    groupBox2.Enabled = false;
                    groupBox3.Enabled = true;
                    groupBox4.Enabled = true;
                    groupBox7.Enabled = false;
                    statusLabel.Text = PointPolygonStatus();
                }
                else if (selectedItems[0] is Segment
                         && selectedItems[1] is Segment)
                {
                    groupBox5.Enabled = false;
                    groupBox6.Enabled = true;
                }
                else
                {
                    groupBox2.Enabled = false;
                    groupBox3.Enabled = false;
                    groupBox4.Enabled = false;
                    statusLabel.Text = "";
                    groupBox5.Enabled = false;
                    groupBox6.Enabled = false;
                    groupBox7.Enabled = false;
                }
            else
            {
                groupBox2.Enabled = false;
                groupBox3.Enabled = false;
                groupBox4.Enabled = false;
                groupBox5.Enabled = false;
                groupBox6.Enabled = false;
                groupBox7.Enabled = false;
                statusLabel.Text = "";
            }
        }

        private string PointPolygonStatus()
        {
            IEnumerable<IPrimitive> selectedItems = primitivesListBox.SelectedItems.OfType<IPrimitive>();
            Point point = selectedItems.First(primitive => primitive is Point) as Point;
            Polygon polygon = selectedItems.First(primitive => primitive is Polygon) as Polygon;
            string result = point.ToString();
            if (point.PointInConvexPolygon(polygon) && polygon.IsConvex())
                result += " принадлежит выпуклому ";
            else if (point.PointInPolygon(polygon))
                result += " принадлежит невыпуклому ";
            else
                result += " не принадлежит ";
            result += polygon.ToString();
            return result;
        }

        private string PointSegmentStatus()
        {
            IEnumerable<IPrimitive> selectedItems = primitivesListBox.SelectedItems.OfType<IPrimitive>();
            Point point = selectedItems.First(primitive => primitive is Point) as Point;
            Segment segment = selectedItems.First(primitive => primitive is Segment) as Segment;
            string result = point.ToString();
            switch (point.PointSegmentPosition(segment))
            {
                case Algorithms.PointSegmentRelation.Lies:
                    result += " лежит на ";
                    break;
                case Algorithms.PointSegmentRelation.Left:
                    result += " слева от ";
                    break;
                case Algorithms.PointSegmentRelation.Right:
                    result += " справа от ";
                    break;
            }
            result += segment.ToString();
            return result;
        }

        private void translateButton_Click(object sender, EventArgs e)
        {
            // Перемещение выделенного полигона.
            float dx, dy;
            if (!float.TryParse(translateXTextBox.Text, out dx))
                return;
            if (!float.TryParse(translateYTextBox.Text, out dy))
                return;
            Polygon polygon = primitivesListBox.SelectedItem as Polygon;
            primitivesListBox.Items[primitivesListBox.Items.IndexOf(polygon)] = polygon.Apply(AffineMatrices.Translate(-dx, -dy));
            Redraw();
        }

        private void rotateButton_Click(object sender, EventArgs e)
        {
            // Вращение выделенного полигона вокруг своего центра либо вокруг выделенной точки.
            float angle;
            if (!float.TryParse(rotateAngleTextBox.Text, out angle))
                return;
            IEnumerable<IPrimitive> selectedItems = primitivesListBox.SelectedItems.OfType<IPrimitive>();
            Polygon polygon = selectedItems.First(primitive => primitive is Polygon) as Polygon;
            Point rotationPoint = selectedItems.Any(primitive => primitive is Point)
                ? selectedItems.First(primitive => primitive is Point) as Point
                : polygon.Center;
            primitivesListBox.Items[primitivesListBox.Items.IndexOf(polygon)] = polygon.Apply(AffineMatrices
                .Translate(rotationPoint.x, rotationPoint.y)
                .Rotate(angle)
                .Translate(-rotationPoint.x, -rotationPoint.y)
                );
            Redraw();
        }

        private void scaleButton_Click(object sender, EventArgs e)
        {
            float kx, ky;
            if (!float.TryParse(scaleXTextBox.Text, out kx))
                return;
            if (!float.TryParse(scaleYTextBox.Text, out ky))
                return;
            IEnumerable<IPrimitive> selectedItems = primitivesListBox.SelectedItems.OfType<IPrimitive>();
            Polygon polygon = selectedItems.First(primitive => primitive is Polygon) as Polygon;
            Point scalingPoint = selectedItems.Any(primitive => primitive is Point)
                ? selectedItems.First(primitive => primitive is Point) as Point
                : polygon.Center;
            primitivesListBox.Items[primitivesListBox.Items.IndexOf(polygon)] = polygon.Apply(AffineMatrices
                .Translate(scalingPoint.x, scalingPoint.y)
                .Scale(1 / kx, 1 / ky)
                .Translate(-scalingPoint.x, -scalingPoint.y)
                );
            Redraw();
        }
        
        public void Clear()
        {
            primitivesListBox.Items.Clear();
            groupBox2.Enabled = false;
            groupBox3.Enabled = false;
            groupBox4.Enabled = false;
            groupBox5.Enabled = false;
            groupBox6.Enabled = false;
            translateXTextBox.Text = string.Empty;
            translateYTextBox.Text = string.Empty;
            rotateAngleTextBox.Text = string.Empty;
            scaleXTextBox.Text = string.Empty;
            scaleYTextBox.Text = string.Empty;
            Redraw();
        }

        private void DisableButtons()
        {
            addPointButton.Enabled = false;
            addSegmentButton.Enabled = false;
            addPolygonButton.Enabled = false;
            clearSceneButton.Enabled = false;
        }

        private void EnableButtons()
        {
            addPointButton.Enabled = true;
            addSegmentButton.Enabled = true;
            addPolygonButton.Enabled = true;
            clearSceneButton.Enabled = true;
        }

        // Обработчик кликов мыши
        private void PictureBoxMouseClick(object sender, MouseEventArgs e)
        {
            switch (mode)
            {
                case Mode.AddPoint:
                    AddPoint((float)e.Location.X, (float)e.Location.Y);
                    break;
                case Mode.AddSegment:
                    clickCount += 1;
                    if (clickCount >= 2)
                    {
                        AddSegment(prevPoint.x, prevPoint.y, (float)e.Location.X, (float)e.Location.Y);
                        clickCount = 0;
                        prevPoint = null;
                    }
                    else
                    {
                        prevPoint = new Point((float)e.Location.X, (float)e.Location.Y);
                    }
                    break;
                case Mode.AddPolygon:
                    if (prevPoint != null)
                    {
                        if (e.Button != MouseButtons.Right)
                        {
                            Segment segment = new Segment(prevPoint.x, prevPoint.y, (float)e.Location.X, (float)e.Location.Y);
                            segment.Draw(graphics);
                            pictureBox.Refresh();
                            segments.AddLast(segment);
                            prevPoint = new Point((float)e.Location.X, (float)e.Location.Y);
                        }
                        else
                        {
                            Segment segment = new Segment(prevPoint.x, prevPoint.y, segments.First.Value.point1.x, segments.First.Value.point1.y);
                            segment.Draw(graphics);
                            pictureBox.Refresh();
                            segments.AddLast(segment);
                            AddPolygon(segments);
                            segments.Clear();
                            prevPoint = null;
                        }
                    }
                    else
                    {
                        if (e.Button == MouseButtons.Left)
                        {
                            prevPoint = new Point((float)e.Location.X, (float)e.Location.Y);
                            segments = new LinkedList<Segment>();
                        }
                    }
                    break;
            }
        }

        public void AddPoint(float x, float y)
        {
            Point point = new Point(x, y);
            point.Draw(graphics);
            pictureBox.Refresh();
            primitivesListBox.Items.Add(point);
            mode = Mode.Select;
            EnableButtons();
        }

        public void AddSegment(float x1, float y1, float x2, float y2)
        {
            Segment segment = new Segment(x1, y1, x2, y2);
            segment.Draw(graphics);
            pictureBox.Refresh();
            primitivesListBox.Items.Add(segment);
            mode = Mode.Select;
            EnableButtons();
        }

        public void AddPolygon(IEnumerable<Segment> segments)
        {
            Polygon polygon = new Polygon(segments);
            primitivesListBox.Items.Add(polygon);
            mode = Mode.Select;
            EnableButtons();
        }

        private void rotate90_Click(object sender, EventArgs e)
        {
            Segment segment = primitivesListBox.SelectedItems.OfType<Segment>().First();
            primitivesListBox.Items[primitivesListBox.Items.IndexOf(segment)] = segment.Apply(AffineMatrices
                .Translate(segment.Center.x, segment.Center.y)
                .Rotate(90)
                .Translate(-segment.Center.x, -segment.Center.y)
                );
            Redraw();
        }

        private void rotateMinus90_Click(object sender, EventArgs e)
        {
            Segment segment = primitivesListBox.SelectedItems.OfType<Segment>().First();
            primitivesListBox.Items[primitivesListBox.Items.IndexOf(segment)] = segment.Apply(AffineMatrices
                .Translate(segment.Center.x, segment.Center.y)
                .Rotate(-90)
                .Translate(-segment.Center.x, -segment.Center.y)
                );
            Redraw();
        }

        private void AddIntersectionPoint_Click(object sender, EventArgs e)
        {
            IEnumerable<IPrimitive> selectedItems = primitivesListBox.SelectedItems.OfType<IPrimitive>();
            Segment ab = selectedItems.First() as Segment;
            Segment cd = selectedItems.Last() as Segment;
            Point intersectionPoint = Algorithms.Intersection(ab.point1, ab.point2, cd.point1, cd.point2);
            if (Algorithms.IsIntersection(ab.point1, ab.point2, cd.point1, cd.point2, intersectionPoint))
                AddPoint(intersectionPoint.x, intersectionPoint.y);                
        }

        private void triangulateButton_Click(object sender, EventArgs e)
        {
            Polygon polygon = primitivesListBox.SelectedItem as Polygon;
            Debug.Print(polygon.Triangulate().Count.ToString());
            primitivesListBox.Items.AddRange(polygon.Triangulate().ToArray());
            primitivesListBox.Items.Remove(polygon);
        }
    }
}